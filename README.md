# estart-dependencies

#### 介绍
其实我们的日常开发中，无论是去找jar的版本依赖、去编写一个工具类的jar或者是一个可运行的jar，背后的配置其实是十分复杂的，有很多需要关心的细节，如仓库、编译源代码、运行jar的目录分离，本项目能够给你的开发带来飞跃的提升，estart-dependencies, 本项目提供了快速接入团队中基类的管理，快速实现打包、包依赖等，简化关于pom的开发【在没有基础的团队至少可以节省好几pd的开发量】

#### 软件架构
软件架构说明

- 架构图
  
- http://naotu.baidu.com/file/3c1e7f450f243d51bfeaf39aa28f6981?token=a98645de54d285cc
  
- estarter-base-pom

  - 基础配置如：仓库的配置，内部继承

    ```xml
      <!-- your maven nexus -->
        <properties>
            <nexus.ip.port>maven.okjiaoyu.cn</nexus.ip.port>
        </properties>
    ```

    

- estarter-jar-importer【组合的版本管理】

  - jar的 导包管理，import后可以 管理通用的版本依赖，是组合模式不是继承哦！解耦了版本依赖以及打包相关的细节【通常我们是指定parent来使用，本pom是通过dependencyManagement import的组合方式来实现 通用jar版本管理】

    ```xml
       <!-- your jar version management -->
    	<dependencyManagement>
            <dependencies>
                <dependency>
                    <groupId>org.estarter</groupId>
                    <artifactId>estarter-jar-importer</artifactId>
                    <version>0.2-SNAPSHOT</version>
                    <scope>import</scope>
                    <type>pom</type>
                </dependency>
            </dependencies>
        </dependencyManagement>
    ```
  
  
  
- estarter-jar-parent【普通jar打包的parent，不关心配置文件、依赖等】

  - jar 的super pom，继承后可以减少配置仓库、jdk、源码编译等

    ```xml
    <parent>
            <groupId>org.estarter</groupId>
            <artifactId>estarter-base-pom</artifactId>
            <version>0.2-SNAPSHOT</version>
        </parent>
    ```

- estarter-runjar-parent【可运行jar的parent，关心配置文件、依赖、静态文件等】

  - 可运行jar的super pom，继承后可以生成springboot等项目的可运行jar，切结构分离

    ```xml
        <!-- 继承后无需关注可运行jar的编译细节 -->
    	<parent>
            <groupId>org.estarter</groupId>
            <artifactId>estarter-runjar-parent</artifactId>
            <version>0.2-SNAPSHOT</version>
        </parent>
    
        <!-- your run main class -->
    	 <properties>
            <main.startup.class>com.noriental.okcalls.Application</main.startup.class>
        </properties>
    
    ```
  ```
  
  ```
  
- 目录概览
  
  - target
  
    - release
  
      - xx.jar 【run jar】
  
      - conf/
  
          - mappers/【mybatis 配置】
        - springs/ 【spring配置】
  
      - lib/ 【copy from lib】
  
      - templates/ 【copy from templates 静态文件目录，供jsp、velocity等静态服务】
  
      - bin/ 【启动脚本 copy from resource scripts】
  
        
  
  runjar编译后结构图如下
  
    ![runjar概要图](https://images.gitee.com/uploads/images/2020/1111/182303_603b4f96_97206.png "runjar-dir.png")
  
  - jsp 类 配置代码
  
    ```java
    package com.noriental.okcalls.config;
    
    import lombok.extern.slf4j.Slf4j;
    import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
    import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
    import org.springframework.context.annotation.Bean;
    import org.springframework.context.annotation.Configuration;
    import org.springframework.util.ResourceUtils;
    import org.springframework.web.servlet.view.InternalResourceViewResolver;
    
    import java.io.File;
    import java.io.FileNotFoundException;
    
    /***
     * *   ____  ___________  ___________           ________   ____  __.  _____ _____.___.
     * *   \   \/  /\______ \ \_   _____/           \_____  \ |    |/ _| /  _  \\__  |   |
     * *    \     /  |    |  \ |    __)     ______   /   |   \|      <  /  /_\  \/   |   |
     * *    /     \  |    `   \|     \     /_____/  /    |    \    |  \/    |    \____   |
     * *   /___/\  \/_______  /\___  /              \_______  /____|__ \____|__  / ______|
     * *    	 \_/        \/     \/                       \/        \/       \/\/
     * *
     * *   功能描述：
     * *
     * *   @DATE    2020-11-11
     * *   @AUTHOR  qiyubin
     ***/
    @Configuration
    @Slf4j
    public class JspConfig {
    
        @Bean
        public InternalResourceViewResolver setupViewResolver() {
            InternalResourceViewResolver resolver = new InternalResourceViewResolver();
            resolver.setPrefix("/");
            resolver.setSuffix(".jsp");
            return resolver;
        }
    
        @Bean
        public ConfigurableServletWebServerFactory serverFactory() {
            //run position
            String position = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
            String url = "";
            //run jar path
            if (position.contains(".jar")) {
                File jarFile = new File(position);
                url = jarFile.getParent();
            } else {
                //ide run path
                try {
                    url = ResourceUtils.getURL("classpath:").getPath();
                } catch (FileNotFoundException e) {
    
                }
            }
            log.info("file classpath: {}", url);
            ConfigurableServletWebServerFactory aFactory = new TomcatServletWebServerFactory();
            aFactory.setDocumentRoot(new File(url + "/templates/"));
            return aFactory;
        }
    
  
    }
    
    ```
  
    


#### 安装教程

1.  修改estarter-base-pom中的仓库 proerties
2.  mvn clean deploy
3.  super pom的modules中包含三个用例 可以自行去掉注释放开打包测试
    1.  pom_jar_import 是一个仅引入包版本管理的例子
    2.  pom_normal_jar 是一个普通jar【通常的工具类等】的例子
    3.  pom-run-jar 是一个运行jar【通常的springboot启动类】的例子，包含一个springboot 集成jsp的例子

#### 使用说明

1.  参考软件架构描述

#### 参与贡献



#### 特技

