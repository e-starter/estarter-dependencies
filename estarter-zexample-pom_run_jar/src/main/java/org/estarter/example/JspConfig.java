package org.estarter.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.io.File;
import java.io.FileNotFoundException;

/***
 * *   ____  ___________  ___________           ________   ____  __.  _____ _____.___.
 * *   \   \/  /\______ \ \_   _____/           \_____  \ |    |/ _| /  _  \\__  |   |
 * *    \     /  |    |  \ |    __)     ______   /   |   \|      <  /  /_\  \/   |   |
 * *    /     \  |    `   \|     \     /_____/  /    |    \    |  \/    |    \____   |
 * *   /___/\  \/_______  /\___  /              \_______  /____|__ \____|__  / ______|
 * *    	 \_/        \/     \/                       \/        \/       \/\/
 * *
 * *   功能描述：
 * *
 * *   @DATE    2020-11-11
 * *   @AUTHOR  qiyubin
 ***/
@Configuration
@Slf4j
public class JspConfig {

    @Bean
    public InternalResourceViewResolver setupViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    public ConfigurableServletWebServerFactory serverFactory() {
        //run position
        String position = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        String url = "";
        //run jar path
        if (position.contains(".jar")) {
            File jarFile = new File(position);
            url = jarFile.getParent();
        } else {
            //ide run path
            try {
                url = ResourceUtils.getURL("classpath:").getPath();
            } catch (FileNotFoundException e) {

            }
        }
        log.info("file classpath: {}", url);
        ConfigurableServletWebServerFactory aFactory = new TomcatServletWebServerFactory();
        aFactory.setDocumentRoot(new File(url + "/templates/"));
        return aFactory;
    }


}
